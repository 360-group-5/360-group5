import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.*;

// TODO: Auto-generated Javadoc
/**
 * The Class GUIcontroller.
 */
public class GUIcontroller extends JFrame{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3021325282994735180L;
	
	/** The controller. */
	private WifiRouter controller;
	
	/** The pusher. */
	private WifiRouter pusher;
	
	private String weatherDisplay = "Current weather:";

	/**
	 * Instantiates a new GUIcontroller.
	 */
	private GUIcontroller(){
		//Default close and title
		setTitle("Weather System GUI");
		setPreferredSize(new Dimension(600, 500));
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//UI portion
		JPanel myPanel1 = new JPanel(new GridLayout(0, 2));
		myPanel1.setBackground(Color.lightGray);
		JPanel myPanel2 = new JPanel(new BorderLayout());
		JLabel myPage = new JLabel("Home");
		myPanel2.add(myPage);
		
		//Home buttons and labels
		JLabel myWeather = new JLabel(weatherDisplay);
		JButton myUpdate = buttonMake("Update Weather");
		JButton myReport = buttonMake("Generate Report");
		JButton mySearch = buttonMake("Search");
		JButton myAlarm = buttonMake("Set Alarm");
		JButton mySettings = buttonMake("Settings");
		
		myPanel1.add(myWeather);
		myPanel1.add(myUpdate);
		myPanel1.add(myReport);
		myPanel1.add(mySearch);
		myPanel1.add(myAlarm);
		myPanel1.add(mySettings);
		
		this.getContentPane().add(myPanel1, BorderLayout.CENTER);
		this.getContentPane().add(myPanel2, BorderLayout.NORTH);
		pack();
		setLocationRelativeTo(null);
	}
	
	/**
	 * Button maker.
	 *
	 * @param theName the name
	 * @return the Jbutton
	 */
	private JButton buttonMake(final String theName) {
        final JButton button = new JButton(theName);
        return button;
    }
	
	/**
	 * Builds the window.
	 */
	public void build() {
		GUIcontroller myWindow = new GUIcontroller();
	}
	
	/**
	 * Change settings option.
	 */
	private void changeSettings() {}
	
	/**
	 * Gets the forecast for the day.
	 *
	 * @return the forecast daily
	 */
	private void getForecastDaily() {}
	
	/**
	 * Gets the forecast long.
	 *
	 * @return the forecast long
	 */
	private void getForecastLong() {}
	
	/**
	 * Syncs weather data.
	 */
	private void syncData() {}
	
	/**
	 * Plots data.
	 */
	private void plotData() {}
	
	/**
	 * Display high weather.
	 */
	private void displayHigh() {}
	
	/**
	 * Display low weather.
	 */
	private void displayLow() {}
	
	/**
	 * Display all weather data.
	 */
	private void displayAllWeatherData() {}
	
}
